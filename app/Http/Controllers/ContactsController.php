<?php

namespace App\Http\Controllers;

use App\Contacts;
use Illuminate\Http\Request;


class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Show the contact us view
        return view('contact');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'number' =>'required|integer|max:10',
            'email' => 'required|string|email|max:255|unique:users',
            'subject' => 'required|string|max:50',
            'message' => 'required|string|max:225',
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Creating all the contact information of users

    }

    public function store(Request $request)
    {
        //Storing all the information from the different users
        $contacts = new Contacts($request->all());
        $contacts->save();
        $contacts->update();
        flash('Contact information sent Successfully!')->important();
        return redirect()->route('index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Showing all data in databse for contacts
        $contact = Contacts::findOrFail($id);
        return view('home', compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function edit(Contacts $contacts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contacts $contacts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contacts  $contacts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contacts $contacts)
    {
        Schema::dropIfExists('contacts');
    }
}
