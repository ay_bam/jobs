<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacts extends Model
{
    //Getting user information
    protected $fillable = [
        'name', 'number', 'email', 'subject', 'message',
    ];
}
