<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('index');})->name('index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contact', 'ContactsController@index')->name('contact');
Route::post('/contact', 'ContactsController@store')->name('store');

Route::get('/services', 'ServicesController@index')->name('services');

Route::get('/gallery', 'GalleryController@index')->name('gallery');


