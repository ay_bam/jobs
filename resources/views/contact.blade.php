<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sika Pe Adjuma</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>

    <!-- //custom-theme -->
    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script type="text/javascript" src="{{asset('/js/jquery-2.1.4.min.js')}}"></script>
    <!-- //js -->
    <link rel="stylesheet" href="{{asset('/css/colorbox.css')}}" />
    <!-- font-awesome-icons -->
    <link href="{{asset('/css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome-icons -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>


</head>
{{--Body--}}

<!-- banner -->
<div class="main_section_agile">
    <div class="w3_agile_banner_top">
        <div class="agile_phone_mail">
            <ul>
                <li><i class="fa fa-phone" aria-hidden="true"></i>+(000) 123 456 232</li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:education@w3layouts.com">Education@w3layouts.com</a></li>
            </ul>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="agileits_w3layouts_banner_nav">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="{{url('/')}}"><i>Sika</i><span>Pe Adjuma</span></a></h1>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="menu--iris">
                    <ul class="nav navbar-nav menu__list">
                        <li class="menu__item menu__item--current"><a href="{{url('/')}}" class="menu__link">Home</a></li>
                        <li class="menu__item"><a href="{{route('services')}}" class="menu__link">Services</a></li>
                        <li class="menu__item"><a href="{{route('gallery')}}" class="menu__link">Gallery</a></li>
                        <li class="menu__item"><a href="{{route('contact')}}" class="menu__link">Contact Us</a></li>
                    </ul>
                    <div class="social_agileinfo2 pull-right">
                        <li><a href="https://www.facebook.com/sikapeadjumaent/" class="w3_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/SikaPeAdjuma" class="w3_twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/sikapeadjuma/" class="w3_instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/sika-pe-adjuma-02b5b0127/" class="w3_linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </div>
                </nav>
            </div>
        </nav>
    </div>
</div>
<!-- banner -->

{{--Body Contents--}}



<!-- mail -->
<div class="contact">
    <div class="container">
        <h3 class="w3l_header w3_agileits_header1">Get in <span>Touch</span></h3>
        <div class="w3layouts_mail_grid">
            <div class="col-md-4 w3layouts_mail_grid_left">
                <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                </div>
                <div class="w3layouts_mail_grid_left2">
                    <h3>Email Us</h3>
                    <a href="mailto:info@example.com">info@example.com</a>
                </div>
            </div>
            <div class="col-md-4 w3layouts_mail_grid_left">
                <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                </div>
                <div class="w3layouts_mail_grid_left2">
                    <h3>Address</h3>
                    <p>PO Box 97845 Baker , Australia</p>
                </div>
            </div>
            <div class="col-md-4 w3layouts_mail_grid_left">
                <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                    <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                </div>
                <div class="w3layouts_mail_grid_left2">
                    <h3>Phone</h3>
                    <p>+(0121) 121 121</p>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="w3_agile_map">
    <iframe src="https://www.google.com/maps/place/Accra/@5.6034757,-0.2665936,11z/data=!4m5!3m4!1s0xfdf9084b2b7a773:0xbed14ed8650e2dd3!8m2!3d5.6037168!4d-0.1869644?hl=en" style="border:0"></iframe>
</div>
<div class="contact">
    <div class="container">
        <h3 class="w3l_header w3_agileits_header1">Contact <span>Info</span></h3>
        <div class="w3layouts_mail_grid">
            <div class="agileits_mail_grid_right agileits_w3layouts_mail_grid_right">
                <div class="agileits_mail_grid_right1 agile_mail_grid_right1">

                    {!! Form::open(array('route' => 'store', 'class' => 'form'))!!}

                    <div class="form-group">
                        {!! Form::label('Your Name') !!}
                        {!! Form::text('name', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>'Your name')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Your Number') !!}
                        {!! Form::text('number', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>'Your number')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Your E-mail Address') !!}
                        {!! Form::text('email', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>'Your e-mail address')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Your Subject') !!}
                        {!! Form::text('subject', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>'Your subject')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('Your Message') !!}
                        {!! Form::textarea('message', null,
                            array('required',
                                  'class'=>'form-control',
                                  'placeholder'=>'Your message')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Contact Us!',
                          array('class'=>'btn btn-primary')) !!}
                    </div>
                    {!! Form::close() !!}
                    </form>
                </div>
            </div>
            <div class="col-md-6 agileits_mail_grid_left agileinfo_mail_grid_left">
                <div class="agileits_mail_grid_lft_grid wthree_mail_grid_lft_grid">
                    <h4>Integer venenatis massa lobortis porta</h4>
                    <ul>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Nulla nec facilisis turpis ac</li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Eget ornare ex nisl nec lacus</li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Ut tortor augue, placerat massa</li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Quisque consectetur sagittis</li>
                        <li><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Curabitur mollis dignissim erat</li>
                    </ul>
                </div>

            </div>
            <div class="col-md-6 agileits_mail_grid_lft_grid1 w3_agileits_mail_grid_lft_grid1">

                <h4><span>Twitter</span> Posts</h4>
                <ul>
                    <li><a href="mailto:info@example.com">@Mark Lii</a> consequat dui id turpis
                        <a href="#">http://example.com</a> auctor tellus eu nisi vehicula consequat.</li>
                    <li><a href="mailto:info@example.com">@Henry Paul</a> sit amet dolor
                        <a href="#">http://example1.com</a> porttitor suscipit orci semper, bibendum.</li>
                    <li><a href="mailto:info@example.com">@John Carl</a> augue diam consequat mi
                        <a href="#">http://example2.com</a> facilisis vitae, euismod vitae orci consequat.</li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //mail -->



<!-- footer -->
<div class="footer">
    <div class="f-bg-w3l">
        <div class="container">
            <div class="col-md-4 w3layouts_footer_grid">
                <h2>Follow <span>Us</span></h2>
                <ul class="social_agileinfo">
                    <li><a href="https://www.facebook.com/sikapeadjumaent/" class="w3_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/SikaPeAdjuma" class="w3_twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/sikapeadjuma/" class="w3_instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/sika-pe-adjuma-02b5b0127/" class="w3_linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="col-md-8 w3layouts_footer_grid">
                <ul class="w3l_footer_nav">
                    <li><a href="{{url('index')}}" class="active">Home</a></li>
                    <li><a href="{{route('services')}}">Services</a></li>
                    <li><a href="{{route('gallery')}}">Gallery</a></li>
                    <li><a href="{{route('contact')}}">Contact Us</a></li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; <?php echo date('Y'); ?> All Rights Reserved | Design by <strong>Phalcon Tech Gh&trade;</strong></p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //footer -->

{{--Flash Message Scripts--}}
<script src="//code.jquery.com/jquery.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
<div class="container">
    @include('flash::message')
    <p>Contact Information Successfully Added. We would contact you soon</p>
</div
<script>
    $('#flash-overlay-modal').modal();
</script>



<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- for bootstrap working -->
<script src="{{asset('/js/bootstrap.js')}}"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
</body>
</html>