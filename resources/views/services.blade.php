<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sika Pe Adjuma</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    {{--Header script--}}
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); }
    </script>

    <!-- //custom-theme -->
    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('/css/JiSlider.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/flexslider.css')}}" type="text/css" media="screen" property="" />
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />

    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">

    <!-- Google fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

</head>
{{--Body--}}

<!-- banner -->
<div class="main_section_agile">
    <div class="w3_agile_banner_top">
        <div class="agile_phone_mail">
            <ul>
                <li><i class="fa fa-phone" aria-hidden="true"></i>+(000) 123 456 232</li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:education@w3layouts.com">Education@w3layouts.com</a></li>
            </ul>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="agileits_w3layouts_banner_nav">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="{{url('/')}}"><i>M</i><span>astering</span></a></h1>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="menu--iris">
                    <ul class="nav navbar-nav menu__list">
                        <li class="menu__item menu__item--current"><a href="{{url('/')}}" class="menu__link">Home</a></li>
                        <li class="menu__item"><a href="{{route('services')}}" class="menu__link">Services</a></li>
                        <li class="menu__item"><a href="{{route('gallery')}}" class="menu__link">Gallery</a></li>
                        <li class="menu__item"><a href="{{route('contact')}}" class="menu__link">Contact Us</a></li>
                    </ul>

                    <div class="social_agileinfo2 pull-right">
                        <li><a href="https://www.facebook.com/sikapeadjumaent/" class="w3_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/SikaPeAdjuma" class="w3_twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/sikapeadjuma/" class="w3_instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/sika-pe-adjuma-02b5b0127/" class="w3_linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </div>
                </nav>
            </div>
        </nav>
    </div>
</div>


<div class="banner1">

    <div class="w3_agileits_service_banner_info">
        <h2>Services</h2>
    </div>
</div>
<!-- //banner -->
<!-- Modal1 -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <div class="signin-form profile">
                    <h3 class="agileinfo_sign">Sign In</h3>
                    <div class="login-form">
                        <form action="#" method="post">
                            <input type="text" name="email" placeholder="E-mail" required="">
                            <input type="password" name="password" placeholder="Password" required="">
                            <div class="tp">
                                <input type="submit" value="Sign In">
                            </div>
                        </form>
                    </div>
                    <div class="login-social-grids">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                    <p><a href="#" data-toggle="modal" data-target="#myModal3" > Don't have an account?</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //Modal1 -->
<!-- Modal2 -->
<div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <div class="signin-form profile">
                    <h3 class="agileinfo_sign">Sign Up</h3>
                    <div class="login-form">
                        <form action="#" method="post">
                            <input type="text" name="name" placeholder="Username" required="">
                            <input type="email" name="email" placeholder="Email" required="">
                            <input type="password" name="password" placeholder="Password" required="">
                            <input type="password" name="password" placeholder="Confirm Password" required="">
                            <input type="submit" value="Sign Up">
                        </form>
                    </div>
                    <p><a href="#"> By clicking register, I agree to your terms</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //Modal2 -->

<!-- services -->
<div class="services two">
    <div class="container">
        <h3 class="w3l_header w3_agileits_header">Offered <span>Services</span></h3>
        <div class="wthree_services_grids">
            <div class="col-md-6 wthree_services_grid_left">
                <h3>THE TOP POPULAR <span>COURSES</span> FOR YOU</h3>
                <h4>Choose your course</h4>
                <p> Sed quis eleifend leo. Phasellus iaculis,
                    metus facilisis gravida dapibus, ligula dolor placerat dolor, eget
                    cursus neque risus quis tortor varius augue ut mauris condimentum dictum.</p>
                <p> Sed quis eleifend leo. Phasellus iaculis,
                    metus facilisis gravida dapibus, ligula dolor placerat dolor, eget
                    cursus neque risus quis tortor varius augue ut mauris condimentum dictum.</p>

            </div>
            <div class="col-md-6 wthree_services_grid_right">
                <div class="col-md-4 agileits_w3layouts_service_grid">
                    <div class="agile_service_grd">
                        <i class="fa fa-share-alt" aria-hidden="true"></i>
                    </div>
                    <h4>Social Media</h4>
                    <p>Tortor varius augue ut mauris dictum.</p>
                </div>
                <div class="col-md-4 agileits_w3layouts_service_grid">
                    <div class="agile_service_grd">
                        <i class="fa fa-laptop" aria-hidden="true"></i>
                    </div>
                    <h4>Business</h4>
                    <p>Tortor varius augue ut mauris dictum.</p>
                </div>
                <div class="col-md-4 agileits_w3layouts_service_grid">
                    <div class="agile_service_grd">
                        <i class="fa fa-camera" aria-hidden="true"></i>
                    </div>
                    <h4>Photography</h4>
                    <p>Tortor varius augue ut mauris dictum.</p>
                </div>
                <div class="col-md-4 agileits_w3layouts_service_grid">
                    <div class="agile_service_grd">
                        <i class="fa fa-database" aria-hidden="true"></i>
                    </div>
                    <h4>Software</h4>
                    <p>Tortor varius augue ut mauris dictum.</p>
                </div>
                <div class="col-md-4 agileits_w3layouts_service_grid">
                    <div class="agile_service_grd">
                        <i class="fa fa-microphone" aria-hidden="true"></i>
                    </div>
                    <h4>Language</h4>
                    <p>Tortor varius augue ut mauris dictum.</p>
                </div>
                <div class="col-md-4 agileits_w3layouts_service_grid">
                    <div class="agile_service_grd">
                        <i class="fa fa-comments-o" aria-hidden="true"></i>
                    </div>
                    <h4>Communication</h4>
                    <p>Tortor varius augue ut mauris dictum.</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //services -->

<!-- stats -->
<div class="stats" id="stats">
    <div class="container">
        <div class="inner_w3l_agile_grids">
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid">
                <i class="fa fa-laptop" aria-hidden="true"></i>
                <p class="counter">45</p>
                <h3>Courses</h3>
            </div>
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid1">
                <i class="fa fa-smile-o" aria-hidden="true"></i>
                <p class="counter">165</p>
                <h3>Members</h3>
            </div>
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid2">
                <i class="fa fa-trophy" aria-hidden="true"></i>
                <p class="counter">563</p>
                <h3>Awards</h3>
            </div>
            <div class="col-md-3 w3layouts_stats_left w3_counter_grid3">
                <i class="fa fa-user" aria-hidden="true"></i>
                <p class="counter">245</p>
                <h3>Instructors</h3>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //stats -->

<!-- stats-bottom -->
<div class="stats-bottom contact">
    <div class="container">
        <h3 class="w3l_header w3_agileits_header">Featured <span>Services</span></h3>
        <div class="agileinfo_services_grids">
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid1">
                    <h4>Best Lab</h4>
                    <p>Phasellus a porttitor metus, vitae ultrices nibh. Sed eu fermentum nunc.</p>
                    <div class="agileinfo_services_grid1_pos">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid1">
                    <h4>Best Teachers</h4>
                    <p>Phasellus a porttitor metus, vitae ultrices nibh. Sed eu fermentum nunc.</p>
                    <div class="agileinfo_services_grid1_pos">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid1">
                    <h4>Low Cost Services</h4>
                    <p>Phasellus a porttitor metus, vitae ultrices nibh. Sed eu fermentum nunc.</p>
                    <div class="agileinfo_services_grid1_pos">
                        <span class="glyphicon glyphicon-retweet" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="w3agile_services_grids">
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid1">
                    <h4>Best Class rooms</h4>
                    <p>Phasellus a porttitor metus, vitae ultrices nibh. Sed eu fermentum nunc.</p>
                    <div class="agileinfo_services_grid1_pos">
                        <span class="glyphicon glyphicon-share" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid1">
                    <h4>Online training</h4>
                    <p>Phasellus a porttitor metus, vitae ultrices nibh. Sed eu fermentum nunc.</p>
                    <div class="agileinfo_services_grid1_pos">
                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 agileinfo_services_grid">
                <div class="agileinfo_services_grid1">
                    <h4>Drawing lessons</h4>
                    <p>Phasellus a porttitor metus, vitae ultrices nibh. Sed eu fermentum nunc.</p>
                    <div class="agileinfo_services_grid1_pos">
                        <span class="glyphicon glyphicon-usd" aria-hidden="true"></span>
                    </div>
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!-- //stats-bottom -->


<!-- footer -->
<div class="footer">
    <div class="f-bg-w3l">
        <div class="container">
            <div class="col-md-4 w3layouts_footer_grid">
                <h2>Follow <span>Us</span></h2>
                <ul class="social_agileinfo">
                    <li><a href="https://www.facebook.com/sikapeadjumaent/" class="w3_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/SikaPeAdjuma" class="w3_twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/sikapeadjuma/" class="w3_instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/sika-pe-adjuma-02b5b0127/" class="w3_linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="col-md-8 w3layouts_footer_grid">
                <ul class="w3l_footer_nav">
                    <li><a href="{{url('index')}}" class="active">Home</a></li>
                    <li><a href="{{route('services')}}">Services</a></li>
                    <li><a href="{{route('gallery')}}">Gallery</a></li>
                    <li><a href="{{route('contact')}}">Contact Us</a></li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; <?php echo date('Y'); ?> All Rights Reserved | Design by <strong>Phalcon Tech Gh&trade;</strong></p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>

<!-- //footer -->
<!-- start-smoth-scrolling -->
<!-- js -->
<script type="text/javascript" src="{{asset('/js/jquery-2.1.4.min.js')}}"></script>

<!-- //js -->
<script src="{{asset('/js/JiSlider.js')}}"></script>

<script>
    $(window).load(function () {
        $('#JiSlider').JiSlider({color: '#fff', start: 3, reverse: true}).addClass('ff')
    })
</script><script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<!-- stats -->
<script src="{{asset('/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('/js/jquery.countup.js')}}"></script>
<script>
    $('.counter').countUp();
</script>

<!-- //stats -->
<!-- //footer -->
<!-- flexSlider -->
<script defer src="{{asset('/js/jquery.flexslider.js')}}"></script>

<script type="text/javascript">
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });
</script>

<!-- //flexSlider -->
<script type="text/javascript" src="{{asset('/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- for bootstrap working -->
<script src="{{asset('/js/bootstrap.js')}}"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
</body>
</html>