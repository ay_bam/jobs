<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sika Pe Adjuma</title>
    <!-- custom-theme -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    {{--Header script--}}
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); }
    </script>

    <!-- //custom-theme -->
    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{asset('/css/JiSlider.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/css/flexslider.css')}}" type="text/css" media="screen" property="" />
    <link href="{{asset('/css/style.css')}}" rel="stylesheet" type="text/css" media="all" />

    <!-- font-awesome-icons -->
    <link href="css/font-awesome.css" rel="stylesheet">

    <!-- Google fonts -->
    <link href="//fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

</head>
{{--Body--}}

<!-- banner -->
<div class="main_section_agile">
    <div class="w3_agile_banner_top">
        <div class="agile_phone_mail">
            <ul>
                <li><i class="fa fa-phone" aria-hidden="true"></i>+(000) 123 456 232</li>
                <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:education@w3layouts.com">Education@w3layouts.com</a></li>
            </ul>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="agileits_w3layouts_banner_nav">
        <nav class="navbar navbar-default">
            <div class="navbar-header navbar-left">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1><a class="navbar-brand" href="{{url('/')}}"><i>M</i><span>astering</span></a></h1>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                <nav class="menu--iris">
                    <ul class="nav navbar-nav menu__list">
                        <li class="menu__item menu__item--current"><a href="{{url('/')}}" class="menu__link">Home</a></li>
                        <li class="menu__item"><a href="{{route('services')}}" class="menu__link">Services</a></li>
                        <li class="menu__item"><a href="{{route('gallery')}}" class="menu__link">Gallery</a></li>
                        <li class="menu__item"><a href="{{route('contact')}}" class="menu__link">Contact Us</a></li>
                    </ul>

                    <div class="social_agileinfo2 pull-right">
                        <li><a href="https://www.facebook.com/sikapeadjumaent/" class="w3_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/SikaPeAdjuma" class="w3_twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/sikapeadjuma/" class="w3_instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/sika-pe-adjuma-02b5b0127/" class="w3_linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                    </div>
                </nav>
            </div>
        </nav>
    </div>
</div>


{{--Body Content--}}


<div class="contact">
    <div class="container">
        <h3 class="w3l_header w3_agileits_header">Our <span>Gallery</span></h3>
        <div class="inner_w3l_agile_grids-gal">
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/1.jpg" class="swipebox"><img src="images/1.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>

                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/2.jpg" class="swipebox"><img src="images/2.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>

                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/3.jpg" class="swipebox"><img src="images/3.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>

                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/4.jpg" class="swipebox"><img src="images/4.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>

                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/5.jpg" class="swipebox"><img src="images/5.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>

                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/6.jpg" class="swipebox"><img src="images/6.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/7.jpg" class="swipebox"><img src="images/7.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/8.jpg" class="swipebox"><img src="images/8.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>
                    </div>
                </a>
            </div>
            <div class="col-md-4 gallery-grid gallery1">
                <a href="images/2.jpg" class="swipebox"><img src="images/2.jpg" class="img-responsive" alt="/">
                    <div class="textbox">
                        <h4>Mastering</h4>
                        <p><i class="fa fa-graduation-cap" aria-hidden="true"></i></p>

                    </div>
                </a>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<!--//gallery-->



<!-- footer -->
<div class="footer">
    <div class="f-bg-w3l">
        <div class="container">
            <div class="col-md-4 w3layouts_footer_grid">
                <h2>Follow <span>Us</span></h2>
                <ul class="social_agileinfo">
                    <li><a href="https://www.facebook.com/sikapeadjumaent/" class="w3_facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/SikaPeAdjuma" class="w3_twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.instagram.com/sikapeadjuma/" class="w3_instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    <li><a href="https://www.linkedin.com/in/sika-pe-adjuma-02b5b0127/" class="w3_linkedin" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
            <div class="col-md-8 w3layouts_footer_grid">
                <ul class="w3l_footer_nav">
                    <li><a href="{{url('index')}}" class="active">Home</a></li>
                    <li><a href="{{route('services')}}">Services</a></li>
                    <li><a href="{{route('gallery')}}">Gallery</a></li>
                    <li><a href="{{route('contact')}}">Contact Us</a></li>
                </ul>
                <p class="copyright text-muted small">Copyright &copy; <?php echo date('Y'); ?> All Rights Reserved | Design by <strong>Phalcon Tech Gh&trade;</strong></p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>

{{--Scripts--}}
<!-- js -->
<script type="text/javascript" src="{{asset('/js/jquery-2.1.4.min.js')}}"></script>
<!-- //js -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="{{asset('/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smooth-scrolling -->
<link rel="stylesheet" href="{{asset('/css/swipebox.css')}}">
<script src="{{asset('/js/jquery.swipebox.min.js')}}"></script>
<script type="text/javascript">
    jQuery(function($) {
        $(".swipebox").swipebox();
    });
</script>


<!-- for bootstrap working -->
<script src="{{asset('/js/bootstrap.js')}}"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //here ends scrolling icon -->
</body>
</html>

